$(function() {
        VWCaddyBuild.Init();
});

var VWCaddyBuild = {
        
        "Page": "Home",
        "KeyCodes": { "tab": 9, "enter": 13, "esc": 27, "space": 32, "end": 35, "home": 36, "left": 37, "up": 38, "right": 39, "down": 40 },
        "Alphabet": ["A", "a", "B", "b", "C", "c", "D", "d", "E", "e", "F", "f", "G", "g", "H", "h", "I", "i", "J", "j", "K", "k", "L", "l", "M", "m", "N", "n", "O", "o", "P", "p", "Q", "q", "R", "r", "S", "s", "T", "t", "U", "u", "V", "v", "W", "w", "X", "x", "Y", "y", "Z", "z"],
        "Numbers": ["0","1","2","3","4","5","6","7","8","9"],
        "CharacterMap": ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","0","1","2","3","4","5","6","7","8","9","~","`","!","@","#","$","%","^","&","*","(",")","-","_","=","+",">",".","<",",","/","?","[","]","{","}","|"],
        "HashMapping": [],
        "ThumbsToLoad": 20,
        "ImagesLoaded": 0,
        "ImageLoadComplete": false,
        "VideosLoaded": 0,
        "VideoLoadComplete": false,
        "AccordionsSet": false,
                
        "Init": function() {
                var _this = this;
                this.BuildEmailAddress();
                this.BuildImageTags();
                this.LoadThumbs({"type": "images"});
                if(this.Page == "Home") this.LoadThumbs({"type": "videos"});
                this.Events();
                this.JsMediaQueries();

                $(window).resize(function() { _this.WindowResize(); });
                $(window).scroll(function() { _this.WindowScroll(); });
        },
        
        "WindowResize": function() {
                this.LoadThumbs({"type": "images"});
                if(this.Page == "Home") this.LoadThumbs({"type": "videos"});
                this.JsMediaQueries();
        },
        
        "WindowScroll": function() {
                this.LoadThumbs({"type": "images"});
                if(this.Page == "Home") this.LoadThumbs({"type": "videos"});
        },
        
        "JsMediaQueries": function() {
                switch(this.GetBreakPoint()) {
                        case "desktop": 
                                this.UnSetAccordions();
                        break;
                        case "768":
                        case "640":
                        case "480":
                        case "320":
                                this.SetAccordions();
                        break;
                }
        },
        
        "BuildEmailAddress": function() {

                var _this = this;
                
                for(var i = 0; i < this.CharacterMap.length; i++ ) {
                        this.HashMapping.push(this.Alphabet[Math.floor(Math.random() * this.Alphabet.length)] + this.Alphabet[Math.floor(Math.random() * this.Alphabet.length)] + this.Numbers[Math.floor(Math.random() * this.Numbers.length)] + this.Numbers[Math.floor(Math.random() * this.Numbers.length)]);
                }
        
                var address =	this.HashMapping[1] + ":" +
                                                this.HashMapping[17] + ":" +
                                                this.HashMapping[4] + ":" +
                                                this.HashMapping[13] + ":" +
                                                this.HashMapping[19] + ":" +
                                                this.HashMapping[14] + ":" +
                                                this.HashMapping[13] + ":" +
                                                this.HashMapping[10] + ":" +
                                                this.HashMapping[4] + ":" +
                                                this.HashMapping[11] + ":" +
                                                this.HashMapping[11] + ":" +
                                                this.HashMapping[24] + ":" +
                                                this.HashMapping[27] + ":" +
                                                this.HashMapping[35] + ":" +
                                                this.HashMapping[34] + ":" +
                                                this.HashMapping[28] + ":" +
                                                this.HashMapping[39] + ":" +
                                                this.HashMapping[6] + ":" +
                                                this.HashMapping[12] + ":" +
                                                this.HashMapping[0] + ":" +
                                                this.HashMapping[8] + ":" +
                                                this.HashMapping[11] + ":" +
                                                this.HashMapping[53] + ":" +
                                                this.HashMapping[2] + ":" +
                                                this.HashMapping[14] + ":" +
                                                this.HashMapping[12];
                                                
                $("#email-contact").attr("href", address);
        
        },

        "BuildImageTags": function() {
            var tags = [], htmlTags = '';

            buildImages.forEach(function(image, index) {
                 image.tags.forEach(function(tag, index) {
                    if(tags.indexOf(tag) == -1) {
                        tags.push(tag);
                    } 
                });
            });

            tags.sort();
            
            tags.forEach(function(tag, index) {
                htmlTags += '<li class="build-picture-tag"><input type="checkbox" value="' + tag + '"/> <span>' + tag + '</span></li>';
            });

            $("#build-picture-tags").html(htmlTags);
        },
        
        "LoadThumbs": function(props) {
        
                var data = (props.type == "images") ? buildImages : buildVideos;
                var container = (props.type == "images") ? "#build-thumbs" : "#build-videos";
                var loadedNum = (props.type == "images") ? this.ImagesLoaded : this.VideosLoaded;
                var loadComplete = (props.type == "images") ? this.ImageLoadComplete : this.VideoLoadComplete;
                var nextSet = 0;
                
                if(($(window).scrollTop() + $(window).height()) >= $(container).offset().top) {
                        if(!$(container).closest(".content-panel").attr("aria-hidden") || $(container).closest(".content-panel").attr("aria-hidden") == "false") {
                                if(!$(".thumb.loading").length) {
                                        if(!loadComplete) {
                                                nextSet = loadedNum + this.ThumbsToLoad;
                                                if(nextSet > data.length) {
                                                        nextSet = data.length;

                                                        if(props.type == "images") {
                                                                this.ImageLoadComplete = true;
                                                        } else {
                                                                this.VideoLoadComplete = true;
                                                        }
                                                }

                                                for(var i = loadedNum; i < nextSet; i++) {
                                                        $(container).append('' +
                                                                '<li class="thumb-holder" data-index="' + i + '">' +
                                                                        '<div class="thumb loading' + ((props.type == "videos") ? " video" : "") + '" tabindex="0" data-index="' + i + '">' +
                                                                                '<img src="' + data[i].thumb + '" alt="" onload="$(this).parent().removeClass(\'loading\');" />' +
                                                                        '</div>' +
                                                                '</li>' +
                                                        '');
                                                }

                                                if(props.type == "images") {
                                                        this.ImagesLoaded += this.ThumbsToLoad;
                                                } else {
                                                        this.VideosLoaded += this.ThumbsToLoad;
                                                }
                                        }
                                }
                        }
                }
                
        },
        
        "SetAccordions": function() {
        
                var _this = this;

                if(!this.AccordionsSet) {
                        $("#content h2").attr({
                                "aria-expanded": "false",
                                "role": "button",
                                "tabindex": "0"
                        });
                        $("#content h2 + .content-panel").hide().attr("aria-hidden", "true");
                
                        $(document).on("click keydown", "#content h2", function(e) {
                                if(_this.AllyClick(e)) {
                                        e.preventDefault();

                                        if($(this).attr("aria-expanded") == "false") {
                                                $(this).attr("aria-expanded", "true");
                                                $("+ .content-panel", this).slideDown(function() {
                                                        $(this).attr("aria-hidden", "false");
                                                        
                                                        _this.LoadThumbs({"type": "images"});
                                                        _this.LoadThumbs({"type": "videos"});
                                                });
                                        } else {
                                                $(this).attr("aria-expanded", "false");
                                                $("+ .content-panel", this).slideUp(function() {
                                                        $(this).attr("aria-hidden", "true");
                                                });
                                        }
                                }
                        });
                        
                        this.AccordionsSet = true;
                }
        
        },
        
        "UnSetAccordions": function() {
                
                var _this = this;
        
                if(this.AccordionsSet) {
                        $("#content h2").removeAttr("aria-expanded role tabindex");
                        $("#content h2 + .content-panel").removeAttr("aria-hidden style");
                
                        $(document).off("click keydown", "#content h2");
                        
                        this.AccordionsSet = false;
                }
                
        },
        
        "Events": function() {

                var _this = this;
                
                $(document).on("click keydown", "#email-contact", function(e) {
                        if(_this.AllyClick(e)) {
                                e.preventDefault();

                                var hash = $(this).attr("href").split(":");
                                var address = '';
                                $.each(hash, function(index, code) {
                                        var codeIndex = _this.HashMapping.indexOf(code);

                                        address += _this.CharacterMap[codeIndex];
                                });

                                window.location.href = "mailto:" + address;
                        }
                });
                
                $(document).on("click keydown", ".thumb", function(e) {
                        if(_this.AllyClick(e)) {
                                e.preventDefault();
                                
                                if($("#media-overlay-outer").attr("aria-hidden") == "true") {
                                        $("#media-overlay-outer").attr("aria-hidden", "false");
                                        $("body").css("overflow", "hidden");
                                }
                                
                                var index = parseInt($(this).attr("data-index"));
                                
                                if($(this).hasClass("video")) {
                                        $("#media-overlay").attr("data-type", "video").html('' +
                                                '<video controls autoplay data-index="' + index + '">' +
                                                        '<source src="' + buildVideos[index].src + '" type="video/mp4">' +
                                                '</video>' +
                                        '');
                                } else {
                                        $("#media-overlay").attr("data-type", "image").html('<img src="' + buildImages[index].original + '" data-index="' + index + '" alt="" />');
                                }
                        }
                });
                
                $(document).on("click keydown", ".media-overlay-button", function(e) {
                        if(_this.AllyClick(e)) {
                                e.preventDefault();
                                
                                var mediaType = $("#media-overlay").attr("data-type");
                                var index = (mediaType == "image") ? parseInt($("#media-overlay img").attr("data-index")) : parseInt($("#media-overlay video").attr("data-index"));
                                var data = (mediaType == "image") ? buildImages : buildVideos;
                                
                                if($(this).hasClass("next")) {
                                        if(index == (data.length - 1)) {
                                                index = 0;
                                        } else {
                                                index++;
                                        }
                                } else {
                                        if(index == 0) {
                                                index = data.length - 1;
                                        } else {
                                                index--;
                                        }
                                }
                                
                                if(mediaType == "image") {
                                        $("#media-overlay").html('<img src="' + data[index].original + '" data-index="' + index + '" alt="" />');
                                } else {
                                        $("#media-overlay").html('' +
                                                '<video controls autoplay data-index="' + index + '">' +
                                                        '<source src="' + data[index].src + '" type="video/mp4">' +
                                                '</video>' +
                                        '');
                                }
                        }
                });
                
                $(document).on("keydown", function(e) {
                        if($("#media-overlay-outer").attr("aria-hidden") == "false") {
                                if(e.keyCode == _this.KeyCodes.left) {
                                        e.preventDefault();
                                        
                                        var mediaType = $("#media-overlay").attr("data-type");
                                        var index = (mediaType == "image") ? parseInt($("#media-overlay img").attr("data-index")) : parseInt($("#media-overlay video").attr("data-index"));
                                        var data = (mediaType == "image") ? buildImages : buildVideos;
                                        
                                        if(index == 0) {
                                                index = data.length - 1;
                                        } else {
                                                index--;
                                        }
                                        
                                        if(mediaType == "image") {
                                                $("#media-overlay").html('<img src="' + data[index].original + '" data-index="' + index + '" alt="" />');
                                        } else {
                                                $("#media-overlay").html('' +
                                                        '<video controls autoplay data-index="' + index + '">' +
                                                                '<source src="' + data[index].src + '" type="video/mp4">' +
                                                        '</video>' +
                                                '');
                                        }
                                }
                                
                                if(e.keyCode == _this.KeyCodes.right) {
                                        e.preventDefault();
                                        
                                        var mediaType = $("#media-overlay").attr("data-type");
                                        var index = (mediaType == "image") ? parseInt($("#media-overlay img").attr("data-index")) : parseInt($("#media-overlay video").attr("data-index"));
                                        var data = (mediaType == "image") ? buildImages : buildVideos;
                                        
                                        if(index == (data.length - 1)) {
                                                index = 0;
                                        } else {
                                                index++;
                                        }
                                        
                                        if(mediaType == "image") {
                                                $("#media-overlay").html('<img src="' + data[index].original + '" data-index="' + index + '" alt="" />');
                                        } else {
                                                $("#media-overlay").html('' +
                                                        '<video controls autoplay data-index="' + index + '">' +
                                                                '<source src="' + data[index].src + '" type="video/mp4">' +
                                                        '</video>' +
                                                '');
                                        }
                                }
                                
                                if(e.keyCode == _this.KeyCodes.esc) {
                                        e.preventDefault();
                                        
                                        $("#media-overlay-outer").attr("aria-hidden", "true");
                                        $("#media-overlay").attr("data-type", "");
                                        $("body").css("overflow", "");
                                        setTimeout(function() {
                                                $("#media-overlay").empty();
                                        }, 400);
                                }
                        }
                });
                
                $(document).on("click keydown", "#media-overlay-close", function(e) {
                        if(_this.AllyClick(e)) {
                                e.preventDefault();
                                
                                $("#media-overlay-outer").attr("aria-hidden", "true");
                                $("#media-overlay").attr("data-type", "");
                                $("body").css("overflow", "");
                                setTimeout(function() {
                                        $("#media-overlay").empty();
                                }, 400);
                        }
                });
                
                $(document).on("click keydown", "#back-to-top", function(e) {
                        if(_this.AllyClick(e)) {
                                e.preventDefault();
                                
                                $("html, body").animate({ scrollTop: 0 }, "slow");
                        }
                });

                $(document).on("change", ".build-picture-tag > input", function(e) {
                    console.log($(this).val());
                });
                
        },
        
        "AllyClick": function(event) {

                if(event.type == "click") {
                        return true;
                } else if(event.type == "keydown" && (event.keyCode == this.KeyCodes.space || event.keyCode == this.KeyCodes.enter)) {
                        return true;
                } else {
                        return false;
                }

        },
        
        "GetBreakPoint": function() {
                return window.getComputedStyle(document.querySelector("body"), ":before").getPropertyValue("content").replace(/"|'/g, "");
        }

};
